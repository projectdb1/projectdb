from flask import Flask

# create a server
app = Flask(__name__)


@app.route("/", methods=["GET"])
def root():
    return "welcome to the python app"


app.run(4000, '0.0.0.0', debug=True)
